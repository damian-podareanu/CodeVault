#ifndef MPITYPES_H
#define MPITYPES_H

#include <mpi.h>

void mpitype_indexed_int(int count, int *displacements, MPI_Datatype *type);
void mpitype_indexed_double(int count, int *displacemnts, MPI_Datatype *type);

void mpitype_box_init(MPI_Datatype *new_type);
void mpitype_box_free(MPI_Datatype *type);

void mpitype_conf_init(MPI_Datatype *new_type);
void mpitype_conf_free(MPI_Datatype *type);

#endif

