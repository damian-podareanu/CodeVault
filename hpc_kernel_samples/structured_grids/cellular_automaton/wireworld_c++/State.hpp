#pragma once 

#include <string>

enum class State : char {
	Empty = ' ',
	ElectronHead = '@',
	ElectronTail = '~',
	Conductor = '#'
};

template <typename E> constexpr auto to_integral(E e) {
	return static_cast<typename std::underlying_type<E>::type>(e);
}

template <typename E, typename I> constexpr auto to_enum(I e) {
	return static_cast<E>(e);
}