#include "MpiSubarray.hpp"

SubarrayDefinition::SubarrayDefinition(
    std::initializer_list<SubarrayDimensionDefinition> saDimDefs) {
	for (const auto& dd : saDimDefs) {
		_sizes.push_back(static_cast<int>(dd.size));
		_subSizes.push_back(static_cast<int>(dd.subSize));
		_starts.push_back(static_cast<int>(dd.start));
	}
}

void MpiSubarray::swap(MpiSubarray& first, MpiSubarray& second) noexcept {
	using std::swap;
	swap(first._type, second._type);
}

MpiSubarray::MpiSubarray(SubarrayDefinition sd) {
	MPI_Type_create_subarray(sd.dims(),     // ndims
	                         sd.sizes(),    // array_of_sizes
	                         sd.subSizes(), // array_of_subsizes
	                         sd.starts(),   // array_of_starts
	                         MPI_ORDER_C,   // order
	                         MPI_CHAR,      // oldtype
	                         &_type         // newtype
	                         );
	MPI_Type_commit(&_type);
}
MpiSubarray::~MpiSubarray() {
	if (_type != MPI_DATATYPE_NULL) { MPI_Type_free(&_type); }
}
MpiSubarray::MpiSubarray(MpiSubarray&& other) noexcept { swap(*this, other); }
MpiSubarray& MpiSubarray::operator=(MpiSubarray&& other) noexcept {
	swap(*this, other);
	return *this;
}