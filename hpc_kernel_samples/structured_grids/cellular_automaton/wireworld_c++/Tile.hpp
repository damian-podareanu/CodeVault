#pragma once

#include <ostream>
#include <vector>

#include "Configuration.hpp"
#include "FileIO.hpp"
#include "MpiEnvironment.hpp"
#include "Util.hpp"

class Tile {
	const MpiEnvironment& _env;
	const Configuration& _cfg;

	const HeaderInfo _header;
	const Size _tileSize;
	const std::size_t _modelWidth;
	std::vector<State> _memoryA;
	std::vector<State> _memoryB;
	State* _model;
	State* _nextModel;

	Tile(const Configuration& cfg, const MpiEnvironment& env);

  public:
	auto& model() { return _model; }
	auto& model() const { return _model; }
	auto& modelWidth() const { return _modelWidth; }
	auto& nextModel() { return _nextModel; }
	auto tileSize() const { return _tileSize; }

	friend std::ostream& operator<<(std::ostream& out, const Tile& t);
	static Tile Read(const Configuration& cfg, const MpiEnvironment& env);
	void write() const;
};
