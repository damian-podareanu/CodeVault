#pragma once

#include <mpi.h>

class MpiEnvironment { // wrapper for creating and destroying the environment
	int _worldRank{-1};
	int _worldSize{-1};
	bool _isMaster{false};

  public:
	int worldRank() const { return _worldRank; }
	int worldSize() const { return _worldSize; }
	bool isMaster() const { return _isMaster; }

	void swap(MpiEnvironment& first, MpiEnvironment& second) noexcept;

	MpiEnvironment(int& argc, char* argv[]);
	~MpiEnvironment();
	MpiEnvironment(MpiEnvironment&) = delete;
	MpiEnvironment& operator=(MpiEnvironment&) = delete;
	MpiEnvironment(MpiEnvironment&& other) noexcept;
	MpiEnvironment& operator=(MpiEnvironment&& other) noexcept;
};
