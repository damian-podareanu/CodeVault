CodeVault: A repository of commonly used HPC codes
================

CodeVault is part of the PRACE-4IP project in WP 7.3.C.


Compilation instructions
-------------

The pre-requisites are:

* CMake 2.8.10 or higher. CMake >= 3.0.0 if you want to build everything.
* A C++ compiler
* For advanced examples, C++11/14, OpenMP support, MPI, ISPC, CUDA, OpenCL ...

Use CMake to create an out-of-source build:

```bash
cd hpc_code_samples
mkdir build
cd build
cmake ..
make
make install # optional
```

This automatically compiles all examples based on the per-experiment pre-requisites. For example, if CUDA is not found, all CUDA-based examples are not compiled.

Automatic building of CodeVault
-------------

To test whether CodeVault dwarfs can be built with a given compiler, please use `test_build.py` script:

```
cd build_files
./test_build.py dwarfs_to_build.json [-j]
```

which uses `gcc`, `g++` and `gfortran` by default. To build with with different compilers,
for exmaple Intel or CLANG or PGI, or different versions of GNU, `compilers.json` configuration file is required.

For example, on Linux, CodeVault and its all individual dwarfs/sub-dwarfs can be built with 
```
cd build_files
./test_build.py dwarfs_to_build.json  compilers_linux_icc_clang_pgi_gcc.json [-j]
```

which will use default CLANG, GNU, Intel and PGI compilers. In case some compilers in the list are not found, 
a build failure will be reported.

  * JSON configuration file `dwarfs_to_build.json` provides a list of paths to dwarfs/sub-dwarfs to build. 
You can create your own configuration file which contains different list of [sub-]dwarfs. 
See `dwarfs_to_build.json` as for an example.
  * JSON configuration file `compilers.json` provides a list of compilers for C++ (`cxx`), C (`cc`), and Fortran (`fortran`).
See `compilers_osx_macposts.json` and `compilers_linux_icc_clang_pgi_gcc.json`
  * Flag `-j` uses all available CPU cores for each compiler set: it builds each dwarf with `make -j 4` with `cpu_count/4` dwarfs in parallel.


Code examples
-------------

__[D3 Spectral Methods]: FFTW__:

This example uses the FFTW3 library to perform a 1D forward and inverse fast Fourier transform (FFT) on a double-precision complex signal. It illustrates intermediate use of FFTW3 plans.

Additional pre-requisites:

* FFTW3 library

__[D3 Spectral Methods]: cuFFT__:

This example uses NVIDIA's CUDA cuFFT library to perform a 1D forward and inverse fast Fourier transform (FFT) on a single-precision complex signal on the GPU. It illustrates the basic use of cuFFT plans and CUDA buffers.

Additional pre-requisites:

* CUDA (includes the cuFFT library)

