
   Code for the parallel solution of the linear Partial Differential Equation

                             Laplacian u = f

   in a quadrilateral two dimensional isoparametric grid, with Dirichlet boundary conditions 

                         u = bc (on the boundary).

  The solution technique is by the finite element method, using bilinear (Q1) elements.

Reference:
        The Finite Element Method Displayed, by Dhatt and Touzot
	
-----------------------------------------------------------------------------------
Usage:
    generate the grid using  the simple sequential grid generation program input
    solve the equation using the parallel MPI-PETSc program main 

  ++ Running the Grid Generator:
        make BOPT=xxxx finput
	finput -xintervals 9 -yintervals 9 -xmin 0.0 -xmax 1.0 -ymin 0.0 -ymax 1.0

  ++ Running the Laplacian Solver:
        make BOPT=xxxx main
	mpirun -np 2 main -f gr99 -show_solution etc

        complete options for main
   
        -show_solution: pipe solution to matlab (visualize with bscript).
        -show_griddata:  print the local index sets and local to global mappings 
        -show_matrix:    visualize the sparsity structure of the stiffness matrix.
        -show_grid:      visualize the global and local grids with numbering.
        -help:   for full description of the run-time options available.
        -f gridfilename: name of file with grid data 
        any PETSc linear solver options

-----------------------------------------------------------------------------------
Directory  Contents
        appctx.h
          -defines the data structures used by the code

	main.c
	  -main part of the code.

	appload.c
	  - get the information from the grid file using database calls.  
            Sets up the local data.

        appalgebra.c
          - setup the linear system and solve it.

	appview.c
	  -utility for viewing the solution and grid.  
           Connects to matlab using the matlab script, bscript.m


The basic philosophy of the codes is to make data-structures and
computations cell-oriented.  The boundary conditions are one
exception, and the flavor of the code for applying boundary conditions
is consequently very different. 


