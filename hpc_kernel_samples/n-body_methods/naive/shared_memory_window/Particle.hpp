#pragma once

#include "Vec3.hpp"

struct Particle {
	double Mass{0};
	Vec3 Location;
	Vec3 Velocity;
};
