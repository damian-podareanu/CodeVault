# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Paul Heinzlreiter <paul.heinzlreiter@risc-software.at>
#
# ==================================================================================================

cmake_minimum_required (VERSION 3.0 FATAL_ERROR)
project ("bhtree_mpi")
include(${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 4_nbody)
endif()
set(NAME ${DWARF_PREFIX}_bhtree_mpi)

enable_language(CXX)
message("** Enabling '${NAME}'")

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
set(CXX11 ${COMPILER_SUPPORTS_CXX11})
set(CXX11_FLAGS -std=c++11)
set(CMAKE_CXX_FLAGS "-std=c++11")

find_package(MPI REQUIRED)

include_directories(${MPI_INCLUDE_PATH} src/datastructures src/simulation)

add_subdirectory(src/datastructures)
add_subdirectory(src/simulation)

add_executable(${NAME} src/mpimain.cpp)
target_link_libraries(${NAME} datastructures simulation ${MPI_LIBRARIES})

if(MPI_COMPILE_FLAGS)
  set_target_properties(${NAME} PROPERTIES
    COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
  set_target_properties(${NAME} PROPERTIES
    LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()

