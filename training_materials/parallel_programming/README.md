Parallel programming training material
======================================

This folder contains various simple code samples for
basic parallel programming examples utilizing both message passing with MPI
and shared memory parallelization with OMP. There are code versions
in C and Fortran programming languages.

Compilation instructions
------------------------
There are no specific requirements for building examples,
just standard make, working MPI environment (for MPI examples) and
OpenMP enabled C or Fortran compiler (for OpenMP examples).

Move to proper subfolder (C or Fortran) and modify the top of the **Makefile**
according to your environment (proper compiler commands and compiler flags).

All examples can be built with simple **make**, **make mpi** builds the MPI
examples and **make omp** OpenMP examples.

Problem assigments
------------------

### Message chain (mpi_msg_chain*)
One dimensional message chain where each MPI task sends data to next one.
Let **ntasks** be the number of the tasks, and **myid** the rank of the current 
MPI task. The program works as follows:

* Every task with a rank less than ntasks-1 sends a message to task myid+1.
For example, task 0 sends a message to task 1.
* The message content is an integer array where each element is initialized to myid.
* The message tag is the receiver’s id number.
* The sender prints out the number of elements it sends and the tag number.
* All tasks with rank >= 1 receive messages.
* Each receiver prints out their myid, and the first element in the received array.

<ol type="a">
  <li>Blocking communication with MPI_Send and MPI_Recv</li>
  <li>Uses the status parameter to find out how much data was received, and
prints out this piece of information for all receivers</li>
  <li>Uses MPI_ANY_TAG when receiving. Prints out the tag of the received
message based on the status message</li>
  <li>Uses MPI_ANY_SOURCE and uses the status information to find out the sender.</li>
  <li>Utilizes MPI_PROC_NULL</li>
  <li>Uses MPI_Sendrecv instead of MPI_Send and MPI_Recv</li>
</ol>

### Collective communication (mpi_collective_communication)
This example shows different routines for collective communication. First, rank 
0 sends an array containing numbers from 0 to 7 to all the
other ranks using collective communication.

Next, we continue with four MPI tasks with following initial data vectors:

|        |    |    |    |    |    |    |    |    |
|--------|----|----|----|----|----|----|----|----|
|Task 0: |  0 |  1 |  2 |  3 |  4 |  5 |  6 |  7 |
|Task 1: |  8 |  9 | 10 | 11 | 12 | 13 | 14 | 15 |
|Task 2: | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 |
|Task 3: | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 |

Values from these data vectors are sent and received with single collective
communication routine, so that the receive buffers will have the following values:

#### Case 1

|        |    |    |    |    |    |    |    |    |
|--------|----|----|----|----|----|----|----|----|
|Task 0: |  0 |  1 | -1 | -1 | -1 | -1 | -1 | -1 |
|Task 1: |  2 |  3 | -1 | -1 | -1 | -1 | -1 | -1 |
|Task 2: |  4 |  5 | -1 | -1 | -1 | -1 | -1 | -1 |
|Task 3: |  6 |  7 | -1 | -1 | -1 | -1 | -1 | -1 |

#### Case 2

|        |    |    |    |    |    |    |    |    |
|--------|----|----|----|----|----|----|----|----|
|Task 0: | -1 | -1 | -1 | -1 | -1 | -1 | -1 | -1 |
|Task 1: |  0 |  8 | 16 | 17 | 24 | 25 | 26 | 27 |
|Task 2: | -1 | -1 | -1 | -1 | -1 | -1 | -1 | -1 |
|Task 3: | -1 | -1 | -1 | -1 | -1 | -1 | -1 | -1 |

#### Case 3

|        |    |    |    |    |    |    |    |    |
|--------|----|----|----|----|----|----|----|----|
|Task 0: |  8 | 10 | 12 | 14 | 16 | 18 | 20 | 22 |
|Task 1: | -1 | -1 | -1 | -1 | -1 | -1 | -1 | -1 |
|Task 2: | 40 | 42 | 44 | 46 | 48 | 50 | 52 | 54 |
|Task 3: | -1 | -1 | -1 | -1 | -1 | -1 | -1 | -1 |

### OpenMP parallel region and data clauses (omp_variables)
There is an OpenMP parallel region around the block where the
variables Var1 and Var2 are printed and manipulated. Illustrates the different 
results one obtains when defining the variables as shared, private or firstprivate.

### Work sharing for a simple loop (omp_sum)
Simple summation of two vectors c = a + b in parallel.

### Dot product (omp_dot)
simple dot product of two vectors parallelizes by using omp parallel or omp do/for pragmas. There are versions having race condition, and avoiding the race condition either with **critical** or **reduction**

### OpenMP library functions (omp_thread_hello)
Simple program that uses omp_get_num_threads and
omp_get_thread_num library functions and prints out the total number of active
threads as well as the id of each thread.
